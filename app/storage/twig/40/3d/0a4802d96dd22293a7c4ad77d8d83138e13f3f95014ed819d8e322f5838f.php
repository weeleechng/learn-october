<?php

/* /var/www/october/themes/demo/partials/footer.htm */
class __TwigTemplate_403d0a4802d96dd22293a7c4ad77d8d83138e13f3f95014ed819d8e322f5838f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer\">
    <div class=\"container\">
        <p class=\"muted credit\">&copy; 2013 Alexey Bobkov &amp; Samuel Georges.</p>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/october/themes/demo/partials/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
