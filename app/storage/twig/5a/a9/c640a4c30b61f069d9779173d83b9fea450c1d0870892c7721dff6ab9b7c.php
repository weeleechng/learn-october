<?php

/* /var/www/october/themes/demo/layouts/static-layout.htm */
class __TwigTemplate_5aa9c640a4c30b61f069d9779173d83b9fea450c1d0870892c7721dff6ab9b7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        <title>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["this"]) ? $context["this"] : null), "page", array()), "title", array()), "html", null, true);
        echo "</title>

        <!-- Bootstrap core CSS -->
        <link 
            rel=\"stylesheet\" 
            href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">

        <!-- Theme CSS -->
        <link href=\"";
        // line 16
        echo $this->env->getExtension('CMS')->themeFilter("assets/css/theme.css");
        echo "\" rel=\"stylesheet\">
    </head>

    <body>
        <div class=\"container\">
            <!-- Static navbar -->
            <div class=\"navbar navbar-default\" role=\"navigation\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" 
                                data-toggle=\"collapse\" 
                                data-target=\".navbar-collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"";
        // line 33
        echo $this->env->getExtension('CMS')->pageFilter("home");
        echo "\">Static Pages Demo</a>
                    </div>
                    <div class=\"navbar-collapse collapse\">
                        ";
        // line 36
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['items'] = $this->getAttribute((isset($context["topMenuLeft"]) ? $context["topMenuLeft"] : null), "menuItems", array())        ;
        $context['__cms_partial_params']['class'] = "nav navbar-nav"        ;
        echo $this->env->getExtension('CMS')->partialFunction("menu-items"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 37
        echo "                        ";
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['items'] = $this->getAttribute((isset($context["topMenuRight"]) ? $context["topMenuRight"] : null), "menuItems", array())        ;
        $context['__cms_partial_params']['class'] = "nav navbar-nav navbar-right"        ;
        echo $this->env->getExtension('CMS')->partialFunction("menu-items"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 38
        echo "                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </div>
        </div> <!-- /container -->

        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-8\">
                    <!-- ";
        // line 46
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('CMS')->componentFunction("staticPage"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        echo " -->
                    ";
        // line 47
        echo $this->env->getExtension('CMS')->pageFunction();
        // line 48
        echo "                </div>

                <div class=\"col-sm-3 col-sm-offset-1\">
                    <div class=\"sidebar\">
                      <div class=\"container-fluid\">
                        <h2>Watch out!</h2>
                        <p>The blog categories plugin used to occupy this part of the page.</p>
                        <p>It got evicted because it was not compatible with PostgreSQL.</p>
                      </div>
                    </div>
                </div>
            </div>
        </div>

        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "/var/www/october/themes/demo/layouts/static-layout.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 48,  94 => 47,  88 => 46,  78 => 38,  71 => 37,  65 => 36,  59 => 33,  39 => 16,  28 => 8,  19 => 1,);
    }
}
