<?php namespace Estorm\DbDemo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTasksTable extends Migration
{

    public function up()
    {
        Schema::create('estorm_dbdemo_tasks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('assigned_to')->nullable();
            $table->text('description')->nullable();
            $table->date('due_date')->nullable();
            $table->boolean('is_complete')->default(false)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('estorm_dbdemo_tasks');
    }

}
