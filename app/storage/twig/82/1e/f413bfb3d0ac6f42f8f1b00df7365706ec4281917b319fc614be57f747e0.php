<?php

/* /var/www/october/themes/demo/partials/page-footer.htm */
class __TwigTemplate_821ef413bfb3d0ac6f42f8f1b00df7365706ec4281917b319fc614be57f747e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer\">
    <div class=\"container\">
        <p class=\"muted credit\">&copy; ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["copyright_year"]) ? $context["copyright_year"] : null), "html", null, true);
        echo " <a href=\"http://www.estorm.com\" target=\"_blank\">estorm International</a></p>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/october/themes/demo/partials/page-footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
