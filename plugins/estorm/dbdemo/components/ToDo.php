<?php namespace Estorm\DbDemo\Components;

use Cms\Classes\ComponentBase;
use Estorm\DbDemo\Models\Task;

class ToDo extends ComponentBase
{
    public $tasks;

    public function componentDetails()
    {
        return [
            'name'        => 'ToDo Component',
            'description' => 'ToDo on Database'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init ()
    {
        // including AJAX
    }

    public function onRun ()
    {
        $this->tasks = Task::all ();
    }

    public function onAddItem ()
    {
        $taskTitle = post ('task_title');
        $taskName = post ('task_name');
        $taskDesc = post ('task_desc');
        $taskDue = post ('task_due');
        $taskCompleted = post ('task_completed');
        
        $task = new Task;
        $task->title = $taskTitle;
        $task->assigned_to = $taskName;
        $task->description = $taskDesc;
        $task->due_date = $taskDue;
        $task->is_complete = $taskCompleted;
        $task->save ();

        $this->page['pageTasks'] = Task::all ();
    }

    public function onEditItem ()
    {
        $id = post ('item_id');
        $task = Task::find ($id);
        $this->page['pageTaskDetail'] = $task;        
    }

    public function onUpdateItem ()
    {
        $id = post ('task_id');
        $taskTitle = post ('task_title');
        $taskName = post ('task_name');
        $taskDesc = post ('task_desc');
        $taskDue = post ('task_due');
        $taskCompleted = post ('task_completed');
        
        $task = Task::find ($id);
        $task->title = $taskTitle;
        $task->assigned_to = $taskName;
        $task->description = $taskDesc;
        $task->due_date = $taskDue;
        $task->is_complete = $taskCompleted;
        $task->save ();

        unset ($this->page['pageTaskDetail']);
        $this->page['pageTasks'] = Task::all ();
    }

    public function onDeleteItem ()
    {
        $id = post ('item_id');
        
        $task = Task::find ($id);
        $task->delete ();
        
        $this->page['pageTasks'] = Task::all ();
    }

}