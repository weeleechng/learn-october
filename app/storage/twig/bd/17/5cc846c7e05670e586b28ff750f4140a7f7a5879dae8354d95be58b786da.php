<?php

/* /var/www/october/plugins/estorm/dbdemo/components/todo/default.htm */
class __TwigTemplate_bd175cc846c7e05670e586b28ff750f4140a7f7a5879dae8354d95be58b786da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('CMS')->startBlock('styles'        );
        // line 2
        echo "<link rel=\"stylesheet\" href=\"";
        echo $this->env->getExtension('CMS')->themeFilter(array(0 => "assets/css/jquery-ui.min.css"));
        echo " \"></link>
";
        // line 1
        echo $this->env->getExtension('CMS')->endBlock(true        );
        // line 4
        echo "<style>
.form-inline .panel .input-group { margin-bottom: 5px; }
.form-inline .panel .input-group .input-group-addon { min-width: 70px; }
</style>
<form
  data-request=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["__SELF__"]) ? $context["__SELF__"] : null), "html", null, true);
        echo "::onUpdate\"
  data-request-success=\"\$('#inputTitle').val(''); \$('#inputName').val(''); \$('#inputDesc').val(''); \$('#inputDate').val(''); \$('#inputComplete').attr('checked', false);\"
  data-request-update=\"'";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["__SELF__"]) ? $context["__SELF__"] : null), "html", null, true);
        echo "::tasklist': '#result'\"
  class=\"form-inline\"
  id=\"form_todo\"
>
  <div class=\"panel panel-default\">
    <div class=\"panel-heading\">
      <h3 class=\"panel-title\">Too Doo</h3>
    </div>
    <div class=\"panel-body\">
      <div id=\"task_formfields\">
        ";
        // line 21
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction(((isset($context["__SELF__"]) ? $context["__SELF__"] : null) . "::formfields")        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 22
        echo "      </div>
    </div>

    ";
        // line 25
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['pageTasks'] = $this->getAttribute((isset($context["__SELF__"]) ? $context["__SELF__"] : null), "tasks", array())        ;
        echo $this->env->getExtension('CMS')->partialFunction(((isset($context["__SELF__"]) ? $context["__SELF__"] : null) . "::tasklist")        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 26
        echo "  </div>
</form>

";
    }

    public function getTemplateName()
    {
        return "/var/www/october/plugins/estorm/dbdemo/components/todo/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 26,  62 => 25,  57 => 22,  53 => 21,  40 => 11,  35 => 9,  28 => 4,  26 => 1,  21 => 2,  19 => 1,);
    }
}
