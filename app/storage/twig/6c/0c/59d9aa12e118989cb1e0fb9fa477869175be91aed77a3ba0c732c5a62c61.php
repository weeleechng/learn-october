<?php

/* /var/www/october/themes/demo/pages/error.htm */
class __TwigTemplate_6c0c59d9aa12e118989cb1e0fb9fa477869175be91aed77a3ba0c732c5a62c61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Curse you, Perry the Platypus...!</h1>
        <p>We're sorry, but something went wrong behind the scene and the page cannot be displayed.</p>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/october/themes/demo/pages/error.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
