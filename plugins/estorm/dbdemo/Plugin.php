<?php namespace Estorm\DbDemo;

use System\Classes\PluginBase;

/**
 * DbDemo Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'DbDemo',
            'description' => 'Database connection demo',
            'author'      => 'Estorm',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerComponents ()
    {
        return [
            'Estorm\DbDemo\Components\ToDo' => 'estormToDo'
        ];
    }

}
