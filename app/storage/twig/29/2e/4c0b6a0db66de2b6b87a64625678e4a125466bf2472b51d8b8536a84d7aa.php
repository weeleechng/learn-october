<?php

/* /var/www/october/plugins/estorm/dbdemo/components/todo/tasks.htm */
class __TwigTemplate_292e4c0b6a0db66de2b6b87a64625678e4a125466bf2472b51d8b8536a84d7aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"list-group\" id=\"result\">
";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pageTasks"]) ? $context["pageTasks"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
            // line 3
            echo "  <li class=\"list-group-item\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "title", array()), "html", null, true);
            echo " 
    <button 
      data-request-confirm=\"Delete task - ";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "title", array()), "html", null, true);
            echo " - sure you?\" 
      data-request-data=\"item_id: ";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute($context["task"], "id", array()), "html", null, true);
            echo "\" 
      data-request=\"";
            // line 7
            echo twig_escape_filter($this->env, (isset($context["__SELF__"]) ? $context["__SELF__"] : null), "html", null, true);
            echo "::onDeleteItem\" 
      data-request-update=\"'";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["__SELF__"]) ? $context["__SELF__"] : null), "html", null, true);
            echo "::tasks': '#result'\"
      class=\"close pull-right\">&times;</button>
  </li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "/var/www/october/plugins/estorm/dbdemo/components/todo/tasks.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 12,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  26 => 3,  22 => 2,  19 => 1,);
    }
}
